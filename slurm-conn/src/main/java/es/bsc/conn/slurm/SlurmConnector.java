package es.bsc.conn.slurm;

import es.bsc.conn.Connector;
import es.bsc.conn.clients.exceptions.ConnClientException;
import es.bsc.conn.clients.slurm.JobDescription;
import es.bsc.conn.clients.slurm.SlurmClient;
import es.bsc.conn.exceptions.ConnException;
import es.bsc.conn.loggers.Loggers;
import es.bsc.conn.types.HardwareDescription;
import es.bsc.conn.types.InstallationDescription;
import es.bsc.conn.types.Processor;
import es.bsc.conn.types.SoftwareDescription;
import es.bsc.conn.types.StarterCommand;
import es.bsc.conn.types.VirtualResource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Implementation of SLURM Connector.
 */
public class SlurmConnector extends Connector {

    // Logger
    private static final Logger LOGGER = LogManager.getLogger(Loggers.SLURM);
    private static final boolean DEBUG = LOGGER.isDebugEnabled();

    // Job Constants
    private static final String RUNNING = "RUNNING";
    private static final String FAILED = "FAILED";

    // Timer constants
    private static final long POLLING_INTERVAL = 5;
    private static final int TIMEOUT = 1_800;

    // Properties values
    private static final String JOB_STATE = "JobState";

    // Worker command related constants
    private static final String JAVA_PROP_DEPLOYMENT_ID = "compss.uuid";
    private static final String NIO_WORKER_STARTER_REL_PATH =
        File.separator + "Runtime" + File.separator + "scripts" + File.separator + "system" + File.separator
            + "adaptors" + File.separator + "nio" + File.separator + "persistent_worker_starter.sh";

    // Information about requests
    private final Map<String, HardwareDescription> vmidToHardwareRequest;
    private final Map<String, SoftwareDescription> vmidToSoftwareRequest;
    private final Map<String, String> vmidToHostName;

    // VMM Client
    private final String logDir;
    private final SlurmClient client;
    private final String network;
    private final boolean memInRequest;

    private int currentNodes;


    /**
     * Initializes the SLRUM connector with the given properties.
     * 
     * @param props Internal properties.
     * @throws ConnException When an initialization error occurs.
     */
    public SlurmConnector(Map<String, String> props) throws ConnException {
        super(props);

        this.vmidToHardwareRequest = new HashMap<>();
        this.vmidToSoftwareRequest = new HashMap<>();
        this.vmidToHostName = new HashMap<>();

        // Setup log dir
        String appLogdir = System.getProperty("compss.appLogDir");
        if (appLogdir == null) {
            throw new ConnException("[Connector] Unable to get app log dir");
        }
        File f = new File(appLogdir + File.separator + "slurm-conn-log");
        if (f.mkdirs()) {
            this.logDir = f.getAbsolutePath();
        } else {
            throw new ConnException("[Connector] Unable to create SLURM connector log dir");
        }

        // Setup client
        String masterName = props.get("master_name");
        if (masterName == null || masterName.isEmpty()) {
            throw new ConnException("[Connector] Unable to get master_name. Property is empty");
        }
        boolean ssh = false;
        String sshStr = props.get("slurm_over_ssh");
        if (sshStr != null && !sshStr.isEmpty()) {
            ssh = Boolean.parseBoolean(sshStr);
        }
        boolean expand = true;
        String expandStr = props.get("expand_job");
        if (expandStr != null && !expandStr.isEmpty()) {
            expand = Boolean.parseBoolean(expandStr);
        }
        LOGGER.debug("[Connector] Starting Slurm client (jobName) for master in " + masterName + ", ssh " + ssh
            + " and expand " + expand);
        this.client = new SlurmClient(masterName, ssh, expand);

        // Setup network
        String networkProp = props.get("network");
        if (networkProp == null) {
            this.network = "";
        } else {
            this.network = networkProp;
        }

        // Setup memory
        String memInReqStr = props.get("mem_in_req");
        if (memInReqStr != null && !memInReqStr.isEmpty()) {
            this.memInRequest = Boolean.parseBoolean(memInReqStr);
        } else {
            this.memInRequest = false;
        }

        // Set current nodes to 0
        this.currentNodes = 0;
    }

    @Override
    public Object create(String requestName, HardwareDescription hd, SoftwareDescription sd, Map<String, String> prop,
        StarterCommand starterCMD) throws ConnException {
        try {
            String jobName = requestName;
            this.currentNodes++;
            JobDescription jobDesc = generateJobDescription(hd, sd);
            String execScript = generateExecScript(jobName, hd, sd, prop, starterCMD);
            String jobId = this.client.createCompute(jobDesc, execScript);
            this.vmidToHardwareRequest.put(jobId, hd);
            this.vmidToSoftwareRequest.put(jobId, sd);

            VirtualResource vr = new VirtualResource(jobId, hd, sd, prop);
            return vr.getId();
        } catch (ConnClientException ce) {
            LOGGER.error("[Connector] Exception submitting job for creation", ce);
            this.currentNodes--;
            throw new ConnException(ce);
        } catch (Exception e) {
            LOGGER.error("[Connector] Exception submitting job for node creation", e);
            throw new ConnException(e);
        }
    }

    @Override
    public Object[] createMultiple(int replicas, String requestName, HardwareDescription hd, SoftwareDescription sd,
        Map<String, String> prop, StarterCommand starterCMD) throws ConnException {
        Object[] envIds = new Object[replicas];
        for (int i = 0; i < replicas; i++) {
            envIds[i] = create(requestName, hd, sd, prop, starterCMD);
        }
        return envIds;
    }

    private String generateExecScript(String jobName, HardwareDescription hd, SoftwareDescription sd,
        Map<String, String> prop, StarterCommand starterCMD) throws ConnException {

        // Add STD flags. Something similar to: -e logDir/jobName.err -o logDir/jobName.out
        String stdFlags = generateFlags(jobName, prop);
        StringBuilder completeCommand = new StringBuilder("#!/bin/sh\n");

        // Add srun if specified
        String launchCommand = prop.get("launch_command");
        if (launchCommand != null && !launchCommand.isEmpty()) {
            LOGGER.debug("[Connector] Adding launch command to the run script");
            completeCommand.append(launchCommand + "$SLURM_JOB_NODELIST ");
        } else {
            LOGGER.debug("[Connector] launch command not found");
        }

        // Add singularity launching
        if (hd.getImageName() != null && !hd.getImageName().isEmpty() && !"None".equals(hd.getImageName())) {
            // singularity case
            completeCommand.append("singularity exec ");

            // check container_ops
            String containerOpts = prop.get("container_opts");
            if (containerOpts != null && !containerOpts.isEmpty()) {
                completeCommand.append(containerOpts + " ");
            }

            // Add image
            completeCommand.append(hd.getImageName());
        }

        // Retrieve Installation Directory
        InstallationDescription instDesc = sd.getInstallation();
        String installDir = instDesc.getInstallDir();
        if (installDir == null || installDir.isEmpty()) {
            installDir = System.getenv("COMPSS_HOME");
            if (installDir == null || installDir.isEmpty()) {
                throw new ConnException("[Connector] Unable to get COMPSs installation directory");
            }
        }

        // Setup start command
        String workingDir = starterCMD.getBaseWorkingDir();
        String deploymentId = System.getProperty(JAVA_PROP_DEPLOYMENT_ID);
        String nodeName = "$SLURM_JOB_NODELIST" + this.network;
        starterCMD.setScriptName(installDir + NIO_WORKER_STARTER_REL_PATH);
        starterCMD.setWorkerName(nodeName);
        starterCMD.setSandboxedWorkingDir(
            workingDir + File.separator + deploymentId + File.separator + nodeName + File.separator);
        starterCMD.setNodeId(String.valueOf(this.client.getInitialNodes() + this.currentNodes));

        // Append persistent_worker script
        try {
            String[] cmd = starterCMD.getStartCommand();
            for (String s : cmd) {
                completeCommand.append(" " + s);
            }
        } catch (Exception e) {
            throw new ConnException(e);
        }

        // Write command in file
        File runScript = new File(this.logDir + File.separator + "run_" + jobName);
        try {
            if (!runScript.createNewFile()) {
                throw new IOException("[Connector] ERROR: File already exists");
            }
            if (!runScript.setExecutable(true)) {
                throw new IOException("[Connector] ERROR: Cannot make the file executable");
            }
        } catch (IOException ioe) {
            throw new ConnException("[Connector] Exception creating script", ioe);
        }

        try (FileOutputStream fos = new FileOutputStream(runScript)) {
            fos.write(completeCommand.toString().getBytes());
            return stdFlags + " " + runScript.getAbsolutePath();
        } catch (IOException e) {
            throw new ConnException("[Connector] Exception writting script", e);
        }
    }

    private String generateFlags(String jobName, Map<String, String> prop) {
        StringBuilder flags = new StringBuilder("--job-name=" + jobName + " -e " + this.logDir + File.separator
            + jobName + ".err -o " + this.logDir + File.separator + jobName + ".out");
        String queue = prop.get("queue");
        if (queue != null && !queue.isEmpty()) {
            flags.append(" -p " + queue);
        }
        String reservation = prop.get("reservation");
        if (reservation != null && !reservation.isEmpty()) {
            flags.append(" --reservation=" + reservation);
        }
        String qos = prop.get("qos");
        if (qos != null && !qos.isEmpty()) {
            flags.append(" --qos=" + qos);
        }
        String constraint = prop.get("constraint");
        if (constraint != null && !constraint.isEmpty()) {
            flags.append(" --constraint=" + constraint);
        }
        return flags.toString();
    }

    private JobDescription generateJobDescription(HardwareDescription hd, SoftwareDescription sd) {
        if (DEBUG) {
            LOGGER.debug("Generating Job description from:");
            LOGGER.debug(" - HD: " + hd);
            LOGGER.debug(" - SD: " + sd);
        }

        Map<String, String> req = new HashMap<>();
        req.put("NumNodes", "1");

        req.put("NumCPUs", Integer.toString(hd.getTotalCPUComputingUnits()));
        if (this.memInRequest && hd.getMemorySize() > 0) {
            req.put("mem", Integer.toString((int) hd.getMemorySize() * 1024));
        }

        // check gpus and set as gres
        int gpuUnits = 0;
        for (Processor p : hd.getProcessors()) {
            if ("GPU".equals(p.getArchitecture())) {
                gpuUnits = gpuUnits + p.getComputingUnits();
            }
        }
        if (gpuUnits > 0) {
            req.put("Gres", Integer.toString(gpuUnits));
        } else if (hd.getTotalGPUComputingUnits() > 0) {
            req.put("Gres", "gpu:" + Integer.toString(hd.getTotalGPUComputingUnits()));
        }

        return new JobDescription(req);
    }

    @Override
    public VirtualResource waitUntilCreation(Object id) throws ConnException {
        LOGGER.debug("[Connector] Waiting for resource creation " + id);
        String jobId = (String) id;
        LOGGER.debug("[Connector] Waiting until node of job " + jobId + " is created");

        try {
            JobDescription jd = this.client.getJobDescription(jobId);
            LOGGER.debug("[Connector] Job State is " + jd.getProperty(JOB_STATE));
            int tries = 0;
            while (jd.getProperty(JOB_STATE) == null || !jd.getProperty(JOB_STATE).equals(RUNNING)) {
                if (jd.getProperty(JOB_STATE).equals(FAILED)) {
                    LOGGER.error("[Connector] Error waiting for VM Creation. Middleware has return an error state");
                    throw new ConnException(
                        "[Connector] Error waiting for VM Creation. Middleware has return an error state");
                }
                if (tries * POLLING_INTERVAL > TIMEOUT) {
                    this.client.cancelJob(jobId);
                    this.vmidToHardwareRequest.remove(jobId);
                    this.vmidToSoftwareRequest.remove(jobId);
                    throw new ConnException("[Connector] Maximum Job creation time reached.");
                }

                tries++;

                Thread.sleep(POLLING_INTERVAL * 1_000);

                jd = this.client.getJobDescription(jobId);
                LOGGER.debug("[Connector] Job State is " + jd.getProperty(JOB_STATE));
            }

            this.client.addNodesToMain(jobId, jd);

            // Create Virtual Resource
            VirtualResource vr = new VirtualResource();
            vr.setId(jobId);
            String resourceName = jd.getNodeList().get(0) + this.network;
            LOGGER.debug("[Connector] Setting resource ip: " + resourceName);
            this.vmidToHostName.put(jobId, resourceName);
            vr.setIp(resourceName);
            vr.setProperties(null);

            HardwareDescription hd = this.vmidToHardwareRequest.get(jobId);
            if (hd == null) {
                throw new ConnException("[Connector] Unregistered hardware description for job " + jobId);
            }
            /*
             * hd.setTotalComputingUnits(vmd.getCpus()); hd.setMemorySize(vmd.getRamMb()/1024);
             * hd.setStorageSize(vmd.getDiskGb()); hd.setImageName(sd.getImage());
             */
            vr.setHd(hd);

            SoftwareDescription sd = vmidToSoftwareRequest.get(jobId);
            if (sd == null) {
                throw new ConnException("[Connector] Unregistered software description for job " + jobId);
            }
            sd.setOperatingSystemType("Linux");
            vr.setSd(sd);
            return vr;
        } catch (ConnClientException | InterruptedException e) {
            LOGGER.error("[Connector] Exception waiting for VM Creation");
            throw new ConnException(e);
        }
    }

    @Override
    public void destroy(Object id) {
        String jobId = (String) id;
        LOGGER.debug("[Connector] Destroying node for job " + jobId);
        try {
            String resourceName = this.vmidToHostName.get(jobId);
            if (resourceName != null && !resourceName.isEmpty()) {
                if (!this.network.isEmpty()) {
                    // erase network for resourceName
                    resourceName = resourceName.substring(0, resourceName.indexOf(this.network));
                }
                LOGGER.debug("[Connector] Deleting node " + resourceName);
                this.client.deleteCompute(resourceName);
            } else {
                LOGGER.debug("[Connector] Node not found cancelling job " + jobId);
            }
            this.vmidToHardwareRequest.remove(jobId);
            this.vmidToSoftwareRequest.remove(jobId);
            this.vmidToHostName.remove(jobId);

        } catch (ConnClientException cce) {
            LOGGER.error("[Connector] Exception waiting for Node Destruction", cce);
        }
        LOGGER.debug("[Connector] Node for job " + jobId + " destroyed.");
        this.currentNodes--;
    }

    @Override
    public float getPriceSlot(VirtualResource virtualResource) {
        return virtualResource.getHd().getPricePerUnit();
    }

    @Override
    public void close() {
        // Nothing to do
    }

}
