package es.bsc.conn.direct;

import es.bsc.conn.Connector;
import es.bsc.conn.exceptions.ConnException;
import es.bsc.conn.loggers.Loggers;
import es.bsc.conn.types.HardwareDescription;
import es.bsc.conn.types.SoftwareDescription;
import es.bsc.conn.types.StarterCommand;
import es.bsc.conn.types.VirtualResource;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Implementation of a Direct Connector.
 */
public class Direct extends Connector {

    // Logger
    private static final Logger LOGGER = LogManager.getLogger(Loggers.DIRECT);
    private static final boolean DEBUG = LOGGER.isDebugEnabled();

    // Timer
    private static final int CREATION_TIME = 15_000; // ms

    // Information about requests
    private final Map<TestMachineId, HardwareDescription> idToHardwareRequest;
    private final Map<TestMachineId, SoftwareDescription> idToSoftwareRequest;


    /**
     * Initializes the Dummy connector with the given properties.
     * 
     * @param props Internal properties.
     * @throws ConnException When an initialization error occurs.
     */
    public Direct(Map<String, String> props) throws ConnException {
        super(props);

        this.idToHardwareRequest = Collections.synchronizedMap(new HashMap<>());
        this.idToSoftwareRequest = Collections.synchronizedMap(new HashMap<>());
    }

    @Override
    public Object create(String requestName, HardwareDescription hd, SoftwareDescription sd, Map<String, String> prop,
        StarterCommand starterCMD) throws ConnException {

        LOGGER.info("Creating VirtualResource with name " + requestName);
        if (DEBUG) {
            LOGGER.debug("- Associated Hardware Description: " + hd);
            LOGGER.debug("- Associated Software Description: " + sd);
        }

        TestMachineId tid = new TestMachineId();
        this.idToHardwareRequest.put(tid, hd);
        this.idToSoftwareRequest.put(tid, sd);

        LOGGER.info("- VR with name " + requestName + " was assigned to ID = " + tid);
        return tid;

    }

    @Override
    public Object[] createMultiple(int replicas, String requestName, HardwareDescription hd, SoftwareDescription sd,
        Map<String, String> prop, StarterCommand starterCMD) throws ConnException {
        TestMachineId[] envIds = new TestMachineId[replicas];
        for (int i = 0; i < replicas; i++) {
            envIds[i] = (TestMachineId) create(requestName, hd, sd, prop, starterCMD);
        }
        return envIds;
    }

    @Override
    public VirtualResource waitUntilCreation(Object id) throws ConnException {
        // Simulate creation time
        TestMachineId tid = (TestMachineId) id;
        LOGGER.info("Waiting for creation of " + tid);

        try {
            Thread.sleep(CREATION_TIME);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }

        // Build return info
        VirtualResource vr = new VirtualResource();
        vr.setId(id);
        vr.setIp(idToHardwareRequest.get((TestMachineId) id).getType());
        vr.setHd(idToHardwareRequest.get((TestMachineId) id));
        vr.setSd(idToSoftwareRequest.get((TestMachineId) id));

        LOGGER.info("Created machine with ID = " + tid);

        return vr;
    }

    @Override
    public void destroy(Object id) {
        TestMachineId tid = (TestMachineId) id;

        LOGGER.info("Deleting VirtualResource " + tid);
        this.idToHardwareRequest.remove(tid);
        this.idToSoftwareRequest.remove(tid);
    }

    @Override
    public float getPriceSlot(VirtualResource virtualResource) {
        LOGGER.info("Getting price slot");
        return 0.0f;
    }

    @Override
    public void close() {
        LOGGER.info("Closing");
    }


    private static class TestMachineId {

        private static final AtomicInteger NEXT_ID = new AtomicInteger(0);
        private final int id = NEXT_ID.getAndIncrement();


        @Override
        public String toString() {
            return "TestMachineId-" + id;
        }
    }

}
