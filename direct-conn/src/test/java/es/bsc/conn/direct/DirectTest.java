package es.bsc.conn.direct;

import es.bsc.conn.direct.Direct;
import es.bsc.conn.exceptions.ConnException;
import es.bsc.conn.types.HardwareDescription;
import es.bsc.conn.types.SoftwareDescription;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;


public class DirectTest {

    private static final Logger LOGGER = LogManager.getLogger("Console");


    @Test
    public void apiTest() throws Exception {
        LOGGER.info("Hello Testing API code!"); // Display the string.

        Direct d;
        try {
            d = new Direct(new HashMap<String, String>());
        } catch (ConnException ce) {
            LOGGER.error("Exception creating Dummy connector", ce);
            throw ce;
        }
        HardwareDescription hd = new HardwareDescription();
        SoftwareDescription sd = new SoftwareDescription();
        HashMap<String, String> prop = new HashMap<String, String>();

        Object id = null;
        try {
            id = d.create("name", hd, sd, prop, null);
        } catch (ConnException ce) {
            LOGGER.error("Exception creating vm", ce);
            throw ce;
        }

        // Integer id = (Integer) d.create(hd, sd, prop);
        LOGGER.info("VM id: " + id);
        d.close();
    }
}
