package es.bsc.conn.mesos;

import es.bsc.conn.Connector;
import es.bsc.conn.clients.mesos.framework.MesosFramework;
import es.bsc.conn.clients.mesos.framework.exceptions.FrameworkException;
import es.bsc.conn.exceptions.ConnException;
import es.bsc.conn.loggers.Loggers;
import es.bsc.conn.types.HardwareDescription;
import es.bsc.conn.types.SoftwareDescription;
import es.bsc.conn.types.StarterCommand;
import es.bsc.conn.types.VirtualResource;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.mesos.Protos.Resource;
import org.apache.mesos.Protos.Value;


/**
 * Implementation of the Mesos connector.
 */
public class Mesos extends Connector {

    // Properties' names
    private static final String PROP_CPUS = "cpus";
    private static final String PROP_MEM = "mem";
    private static final String PROP_DISK = "disk";
    private static final String PROP_PORTS = "ports";
    private static final String PROP_PRICE = "price";

    // Conversion constants
    private static final double GIGAS_TO_MEGAS = 1024.0;
    private static final String UNDEFINED_IP = "-1.-1.-1.-1";
    private static final long SSH_PORT = 22L;

    // Logger
    private static final Logger logger = LogManager.getLogger(Loggers.MESOS);

    // Mesos Framework Client
    private final MesosFramework framework;

    // Information about resources
    private final Map<String, VirtualResource> resources;


    /**
     * Initializes the Mesos connector with the given properties. A Mesos Framework is started, it will do the
     * communication with Mesos.
     *
     * @param props Internal properties.
     * @throws ConnException When an initialization error occurs.
     */
    public Mesos(Map<String, String> props) throws ConnException {
        super(props);

        logger.info("Initializing MESOS Connector");
        this.resources = new HashMap<>();
        try {
            this.framework = new MesosFramework(props);
        } catch (FrameworkException fe) {
            throw new ConnException(fe);
        }
    }

    @Override
    public Object create(String requestName, HardwareDescription hd, SoftwareDescription sd, Map<String, String> prop,
        StarterCommand starterCMD) throws ConnException {

        long adaptorMinPort = Long
            .parseLong(getProperty(hd.getImageProperties(), PROP_ADAPTOR_MIN_PORT, Integer.toString(DEFAULT_MIN_PORT)));
        long adaptorMaxPort = Long
            .parseLong(getProperty(hd.getImageProperties(), PROP_ADAPTOR_MAX_PORT, Integer.toString(DEFAULT_MAX_PORT)));

        List<Resource> res = new LinkedList<>();
        Value.Ranges ports = buildRanges(adaptorMinPort, adaptorMaxPort);
        res.add(buildResource(PROP_CPUS, hd.getTotalCPUComputingUnits()));
        res.add(buildResource(PROP_MEM, GIGAS_TO_MEGAS * hd.getMemorySize()));
        res.add(buildResource(PROP_DISK, GIGAS_TO_MEGAS * hd.getStorageSize()));
        res.add(buildResource(PROP_PORTS, ports));

        String newId = this.framework.requestWorker(this.appName, hd.getImageName(), res);
        this.resources.put(newId, new VirtualResource((String) newId, hd, sd, prop));

        return newId;
    }

    @Override
    public Object[] createMultiple(int replicas, String requestName, HardwareDescription hd, SoftwareDescription sd,
        Map<String, String> prop, StarterCommand starterCMD) throws ConnException {
        String[] envIds = new String[replicas];
        for (int i = 0; i < replicas; i++) {
            envIds[i] = (String) create(requestName, hd, sd, prop, starterCMD);

        }
        return envIds;
    }

    @Override
    public VirtualResource waitUntilCreation(Object id) throws ConnException {
        String identifier = (String) id;
        if (!this.resources.containsKey(identifier)) {
            throw new ConnException("This identifier does not exist " + identifier);
        }
        VirtualResource vr = this.resources.get(identifier);
        String ip = this.framework.waitWorkerUntilRunning(identifier);
        if (UNDEFINED_IP.equals(ip)) {
            throw new ConnException("Could not wait until creation of worker " + id);
        }
        vr.setIp(ip);
        return vr;
    }

    @Override
    public float getPriceSlot(VirtualResource vr) {
        if (vr.getProperties().containsKey(PROP_PRICE)) {
            return Float.parseFloat(vr.getProperties().get(PROP_PRICE));
        }
        return 0.0f;
    }

    @Override
    public void destroy(Object id) {
        String identifier = (String) id;
        this.resources.remove(identifier);
        this.framework.removeWorker(identifier);
    }

    @Override
    public void close() {
        this.framework.stop();
    }

    private String getProperty(Map<String, String> props, String property, String defaultValue) {
        String value = props.get(property);
        return (value != null && !value.isEmpty()) ? value : defaultValue;
    }

    private Value.Range buildRange(long begin, long end) {
        return Value.Range.newBuilder().setBegin(begin).setEnd(end).build();
    }

    private Value.Scalar buildScalar(double value) {
        return Value.Scalar.newBuilder().setValue(value).build();
    }

    private Value.Ranges buildRanges(long begin, long end) {
        Value.Ranges.Builder ranges = Value.Ranges.newBuilder().addRange(buildRange(SSH_PORT, SSH_PORT));
        if (begin > 0L && end > 0L) {
            ranges.addRange(buildRange(begin, end));
        }
        return ranges.build();
    }

    private Resource buildResource(String name, Value.Ranges ranges) {
        return Resource.newBuilder().setName(name).setType(Value.Type.RANGES).setRanges(ranges).build();
    }

    private Resource buildResource(String name, double value) {
        return Resource.newBuilder().setName(name).setType(Value.Type.SCALAR).setScalar(buildScalar(value)).build();
    }

}
