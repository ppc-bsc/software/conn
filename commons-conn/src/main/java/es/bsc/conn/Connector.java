package es.bsc.conn;

import es.bsc.conn.exceptions.ConnException;
import es.bsc.conn.types.HardwareDescription;
import es.bsc.conn.types.SoftwareDescription;
import es.bsc.conn.types.StarterCommand;
import es.bsc.conn.types.VirtualResource;

import java.util.Map;


/**
 * Representation of a generic Cloud Connector.
 */
public abstract class Connector {

    // Properties' names
    protected static final String PROP_APP_NAME = "app-name";
    protected static final String PROP_SERVER = "Server";
    protected static final String PROP_TIME_SLOT = "time-slot";
    protected static final String PROP_ADAPTOR_MAX_PORT = "adaptor-max-port";
    protected static final String PROP_ADAPTOR_MIN_PORT = "adaptor-min-port";
    protected static final String PROP_KEYPAIR_NAME = "vm-keypair-name";
    protected static final String PROP_KEYPAIR_LOC = "vm-keypair-location";
    protected static final String PROP_MAX_VM_CREATION_TIME = "max-vm-creation-time";
    protected static final String PROP_MAX_CONNECTION_ERRORS = "max-connection-errors";
    protected static final String PROP_AUTOMATIC_SCALING = "automatic-scaling";

    // Constants
    protected static final long MIN_TO_S = 60;
    protected static final long S_TO_MS = 1_000;

    // Properties' default values
    protected static final String DEFAULT_APP_NAME = "default-app";
    protected static final String DEFAULT_SERVER = null;
    protected static final long DEFAULT_TIME_SLOT = 5 * MIN_TO_S * S_TO_MS; // MS
    protected static final int DEFAULT_MAX_PORT = -1;
    protected static final int DEFAULT_MIN_PORT = -1;
    protected static final String DEFAULT_KEYPAIR_NAME = "";
    protected static final String DEFAULT_KEYPAIR_LOC = "";
    protected static final long DEFAULT_VM_CREATION_TIME = 10 * MIN_TO_S * S_TO_MS; // MS
    protected static final int DEFAULT_VM_CONNECTION_ERRORS = 3; // Number of maximum errors
    protected static final boolean DEFAULT_AUTOMATIC_SCALING = true;

    // Properties
    protected final String appName;
    protected final String server;
    protected final long timeSlot; // MS
    protected final String keypairName;
    protected final String keypairLoc;
    protected final long maxVMCreationTime; // MS
    protected final int maxVMConnectionErrors;
    protected final boolean automaticScaling;


    /**
     * Initializes the common connector properties.
     * 
     * @param props Specific connector properties.
     * @throws ConnException Exception when starting the connector.
     */
    public Connector(Map<String, String> props) throws ConnException {
        // Parse generic values from properties
        String propAppName = props.get(PROP_APP_NAME);
        if (propAppName != null && !propAppName.isEmpty()) {
            this.appName = propAppName;
        } else {
            this.appName = DEFAULT_APP_NAME;
        }

        String propServer = props.get(PROP_SERVER);
        if (propServer != null && !propServer.isEmpty()) {
            this.server = propServer;
        } else {
            this.server = DEFAULT_SERVER;
        }

        String propTimeSlot = props.get(PROP_TIME_SLOT);
        if (propTimeSlot != null && !propTimeSlot.isEmpty()) {
            // Move from Seconds to Minutes
            this.timeSlot = Long.parseLong(propTimeSlot) * MIN_TO_S * S_TO_MS;
        } else {
            this.timeSlot = DEFAULT_TIME_SLOT;
        }

        String propKeypairName = props.get(PROP_KEYPAIR_NAME);
        if (propKeypairName != null && !propKeypairName.isEmpty()) {
            this.keypairName = propKeypairName;
        } else {
            this.keypairName = DEFAULT_KEYPAIR_NAME;
        }

        String propKeypairLoc = props.get(PROP_KEYPAIR_LOC);
        if (propKeypairLoc != null && !propKeypairLoc.isEmpty()) {
            this.keypairLoc = propKeypairLoc;
        } else {
            this.keypairLoc = DEFAULT_KEYPAIR_LOC;
        }

        String propMaxVMCreationTime = props.get(PROP_MAX_VM_CREATION_TIME);
        if (propMaxVMCreationTime != null && !propMaxVMCreationTime.isEmpty()) {
            // Move from seconds to minutes
            this.maxVMCreationTime = Long.parseLong(propMaxVMCreationTime) * MIN_TO_S * S_TO_MS;
        } else {
            this.maxVMCreationTime = DEFAULT_VM_CREATION_TIME;
        }

        String propMaxVMConnectionErrors = props.get(PROP_MAX_CONNECTION_ERRORS);
        if (propMaxVMConnectionErrors != null && !propMaxVMConnectionErrors.isEmpty()) {
            this.maxVMConnectionErrors = Integer.parseInt(propMaxVMConnectionErrors);
        } else {
            this.maxVMConnectionErrors = DEFAULT_VM_CONNECTION_ERRORS;
        }

        String propAutomaticScaling = props.get(PROP_AUTOMATIC_SCALING);
        if (propAutomaticScaling != null && !propAutomaticScaling.isEmpty()) {
            this.automaticScaling = Boolean.parseBoolean(propAutomaticScaling);
        } else {
            this.automaticScaling = DEFAULT_AUTOMATIC_SCALING;
        }
    }

    /**
     * Returns whether the connector supports automatic scaling or not.
     * 
     * @return {@literal true} if the connector supports automatic scaling, {@literal false} otherwise.
     */
    public boolean isAutomaticScalingEnabled() {
        return this.automaticScaling;
    }

    /**
     * Creates a VM with the given description and properties.
     * 
     * @param requestName Name of the associated request.
     * @param hd Hardware description.
     * @param sd Software description.
     * @param prop Specific properties.
     * @param starterCMD Worker starter command.
     * @return the vmId The assigned VM Id.
     * @throws ConnException When an internal error occurs.
     */
    public abstract Object create(String requestName, HardwareDescription hd, SoftwareDescription sd,
        Map<String, String> prop, StarterCommand starterCMD) throws ConnException;

    /**
     * Creates {@code replicas} VMs with the given description and properties.
     * 
     * @param replicas Number of VMs to create.
     * @param requestName Name of the assigned request.
     * @param hd Hardware description.
     * @param sd Software description.
     * @param prop Specific properties.
     * @param starterCMD Worker starter command.
     * @return A list containing the assigned VM Ids.
     * @throws ConnException When an internal error occurs while creating the VMs.
     */
    public abstract Object[] createMultiple(int replicas, String requestName, HardwareDescription hd,
        SoftwareDescription sd, Map<String, String> prop, StarterCommand starterCMD) throws ConnException;

    /**
     * Waits until the VM with the given id {@code id} is created.
     * 
     * @param id VM Id.
     * @return The final VM description.
     * @throws ConnException When an internal error occurs while waiting for the VM creation.
     */
    public abstract VirtualResource waitUntilCreation(Object id) throws ConnException;

    /**
     * Destroys the VM with the given id {@code id}.
     * 
     * @param id VM Id.
     */
    public abstract void destroy(Object id);

    /**
     * Returns the time slot.
     * 
     * @return The time slot.
     */
    public long getTimeSlot() {
        return this.timeSlot;
    }

    /**
     * Returns the price per time slot.
     * 
     * @param virtualResource Associated virtual resource.
     * @return The price per time slot.
     */
    public abstract float getPriceSlot(VirtualResource virtualResource);

    /**
     * Closes the connector interface.
     */
    public abstract void close();

}
