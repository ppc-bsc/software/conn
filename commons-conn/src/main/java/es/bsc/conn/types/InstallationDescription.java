package es.bsc.conn.types;

/**
 * Implementation of the installation description.
 */
public class InstallationDescription {

    private String adaptorName;
    private int minPort;
    private int maxPort;

    private String installDir;

    private String appDir;
    private String classpath;
    private String pythonPath;
    private String libraryPath;
    private String workingDir;

    private int limitOfTasks;


    /**
     * New installation description instance.
     * 
     * @param adaptorName Adaptor name.
     * @param minPort Minimum adaptor port.
     * @param maxPort Maximum adaptor port.
     * @param installDir Installation directory.
     * @param appDir Application directory.
     * @param classpath Application classpath.
     * @param pythonpath Application pythonpath.
     * @param libraryPath Application library path.
     * @param workingDir Worker working directory.
     * @param limitOfTasks Worker's limit of tasks.
     */
    public InstallationDescription(String adaptorName, int minPort, int maxPort, String installDir, String appDir,
        String classpath, String pythonpath, String libraryPath, String workingDir, int limitOfTasks) {

        this.adaptorName = adaptorName;
        this.minPort = minPort;
        this.maxPort = maxPort;

        this.installDir = installDir;

        this.appDir = appDir;
        this.classpath = classpath;
        this.pythonPath = pythonpath;
        this.libraryPath = libraryPath;
        this.workingDir = workingDir;

        this.limitOfTasks = limitOfTasks;
    }

    /**
     * Returns the associated adaptor name.
     * 
     * @return The associated adaptor name.
     */
    public String getAdaptorName() {
        return this.adaptorName;
    }

    /**
     * Modifies the current adaptor name.
     * 
     * @param adaptorName New adaptor name.
     */
    public void setAdaptorName(String adaptorName) {
        this.adaptorName = adaptorName;
    }

    /**
     * Returns the minimum adaptor port.
     * 
     * @return The minimum adaptor port.
     */
    public int getMinPort() {
        return this.minPort;
    }

    /**
     * Sets a new minimum adaptor port.
     * 
     * @param minPort New minimum adaptor port.
     */
    public void setMinPort(int minPort) {
        this.minPort = minPort;
    }

    /**
     * Returns the maximum adaptor port.
     * 
     * @return The maximum adaptor port.
     */
    public int getMaxPort() {
        return this.maxPort;
    }

    /**
     * Sets a new maximum adaptor port.
     * 
     * @param maxPort New maximum adaptor port.
     */
    public void setMaxPort(int maxPort) {
        this.maxPort = maxPort;
    }

    /**
     * Returns the installation directory.
     * 
     * @return The installation directory.
     */
    public String getInstallDir() {
        return this.installDir;
    }

    /**
     * Sets a new installation directory.
     * 
     * @param installDir New installation directory.
     */
    public void setInstallDir(String installDir) {
        this.installDir = installDir;
    }

    /**
     * Returns the application's directory.
     * 
     * @return The application's directory.
     */
    public String getAppDir() {
        return this.appDir;
    }

    /**
     * Sets a new application directory.
     * 
     * @param appDir New application's directory.
     */
    public void setAppDir(String appDir) {
        this.appDir = appDir;
    }

    /**
     * Returns the application's classpath.
     * 
     * @return The application's classpath.
     */
    public String getClasspath() {
        return this.classpath;
    }

    /**
     * Sets a new classpath.
     * 
     * @param classpath New classpath.
     */
    public void setClasspath(String classpath) {
        this.classpath = classpath;
    }

    /**
     * Returns the application's pythonpath.
     * 
     * @return The application's pythonpath.
     */
    public String getPythonPath() {
        return this.pythonPath;
    }

    /**
     * Sets a new pythonpath.
     * 
     * @param pythonPath New pythonpath.
     */
    public void setPythonPath(String pythonPath) {
        this.pythonPath = pythonPath;
    }

    /**
     * Returns the application's library path.
     * 
     * @return The application's library path.
     */
    public String getLibraryPath() {
        return this.libraryPath;
    }

    /**
     * Sets a new library path.
     * 
     * @param libraryPath New library path.
     */
    public void setLibraryPath(String libraryPath) {
        this.libraryPath = libraryPath;
    }

    /**
     * Returns the application's working directory.
     * 
     * @return The application's working directory.
     */
    public String getWorkingDir() {
        return this.workingDir;
    }

    /**
     * Sets a new working directory.
     * 
     * @param workingDir New working directory.
     */
    public void setWorkingDir(String workingDir) {
        this.workingDir = workingDir;
    }

    /**
     * Returns the limit of tasks.
     * 
     * @return The limit of tasks.
     */
    public int getLimitOfTasks() {
        return this.limitOfTasks;
    }

    /**
     * Sets a new limit of tasks.
     * 
     * @param limitOfTasks New limit of tasks.
     */
    public void setLimitOfTasks(int limitOfTasks) {
        this.limitOfTasks = limitOfTasks;
    }

    @Override
    public String toString() {
        // WARN: Does not display all the task fields
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append("adaptor_name = ").append(this.adaptorName).append(", ");
        sb.append("min_port = ").append(this.minPort).append(", ");
        sb.append("max_port = ").append(this.maxPort);
        sb.append("]");

        return sb.toString();
    }

}
