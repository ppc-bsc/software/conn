package es.bsc.conn.types;

import java.util.LinkedList;
import java.util.List;


/**
 * Implementation of a Virtual Resource Software Description.
 */
public class SoftwareDescription {

    // Operating System
    protected String operatingSystemType = BasicTypes.UNASSIGNED_STR;
    protected String operatingSystemDistribution = BasicTypes.UNASSIGNED_STR;
    protected String operatingSystemVersion = BasicTypes.UNASSIGNED_STR;

    // Applications
    protected List<String> appSoftware;

    protected InstallationDescription installation;


    /**
     * Creates a new software description with empty values.
     */
    public SoftwareDescription() {
        this.appSoftware = new LinkedList<>();
        this.installation = null;
    }

    /**
     * Creates a new software description from the given values.
     * 
     * @param operatingSystemType Operating system type.
     * @param operatingSystemDistribution Operating system distribution.
     * @param operatingSystemVersion Operating system version.
     * @param appSoftware List of application software.
     * @param installation Installation details.
     */
    public SoftwareDescription(String operatingSystemType, String operatingSystemDistribution,
        String operatingSystemVersion, List<String> appSoftware, InstallationDescription installation) {

        this.operatingSystemType = operatingSystemType;
        this.operatingSystemDistribution = operatingSystemDistribution;
        this.operatingSystemVersion = operatingSystemVersion;
        this.appSoftware = appSoftware;
        this.installation = installation;
    }

    /**
     * Returns the operating system type.
     * 
     * @return The operating system type.
     */
    public String getOperatingSystemType() {
        return this.operatingSystemType;
    }

    /**
     * Sets the operating system type.
     * 
     * @param osType New operating system type.
     */
    public void setOperatingSystemType(String osType) {
        this.operatingSystemType = osType;
    }

    /**
     * Returns the operating system distribution.
     * 
     * @return The operating system distribution.
     */
    public String getOperatingSystemDistribution() {
        return this.operatingSystemDistribution;
    }

    /**
     * Sets the operating system distribution.
     * 
     * @param osDistribution New operating system distribution.
     */
    public void setOperatingSystemDistribution(String osDistribution) {
        this.operatingSystemDistribution = osDistribution;
    }

    /**
     * Returns the operating system version.
     * 
     * @return The operating system version.
     */
    public String getOperatingSystemVersion() {
        return this.operatingSystemVersion;
    }

    /**
     * Sets the operating system version.
     * 
     * @param osVersion New operating system version.
     */
    public void setOperatingSystemVersion(String osVersion) {
        this.operatingSystemVersion = osVersion;
    }

    /**
     * Returns the list of installed application software.
     * 
     * @return List of installed application software.
     */
    public List<String> getAppSoftware() {
        return this.appSoftware;
    }

    /**
     * Sets a new list of application software.
     * 
     * @param appSoftware New list of application software.
     */
    public void setAppSoftware(List<String> appSoftware) {
        this.appSoftware = appSoftware;
    }

    /**
     * Returns the COMPSs and Application details for the image.
     * 
     * @return The COMPSs and Application details for the image.
     */
    public InstallationDescription getInstallation() {
        return this.installation;
    }

    /**
     * Sets a new set of installation details for COMPSs and the application.
     * 
     * @param installation Details of the installation of COMPSs and the application.
     */
    public void setInstallation(InstallationDescription installation) {
        this.installation = installation;
    }

    @Override
    public String toString() {
        // WARN: Does not display all the task fields
        StringBuilder sb = new StringBuilder();
        sb.append("SD-").append(hashCode());
        sb.append("[");
        sb.append("os_type = ").append(this.operatingSystemType).append(", ");
        sb.append("os_distro = ").append(this.operatingSystemDistribution).append(", ");
        sb.append("os_version = ").append(this.operatingSystemVersion).append(", ");
        sb.append("install_desc = [").append(this.installation).append("]");
        sb.append("]");

        return sb.toString();
    }
}
