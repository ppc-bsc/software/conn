package es.bsc.conn.types;

/**
 * Representation of a Processor.
 */
public class Processor {

    private String name = BasicTypes.UNASSIGNED_STR;
    private int computingUnits = BasicTypes.ZERO_INT;
    private String type = BasicTypes.UNASSIGNED_PROCESSOR_TYPE;
    private float internalMemory = BasicTypes.UNASSIGNED_FLOAT;
    private float speed = BasicTypes.UNASSIGNED_FLOAT;
    private String architecture = BasicTypes.UNASSIGNED_STR;
    private String propName = BasicTypes.UNASSIGNED_STR;
    private String propValue = BasicTypes.UNASSIGNED_STR;


    /**
     * Instantiates a default processor.
     */
    public Processor() {
        // All default values are already set
    }

    /**
     * Instantiates a processor with a given name (the rest of the parameters are set by default).
     * 
     * @param name Processor's name.
     */
    public Processor(String name) {
        this.setName(name);
    }

    /**
     * Instantiates a processor with a given name and computing units (the rest of the parameters are set by default).
     * 
     * @param name Processor's name.
     * @param cu Number of computing units.
     */
    public Processor(String name, int cu) {
        this.setName(name);
        this.setComputingUnits(cu);
    }

    /**
     * Instantiates a processor with a given name, computing units, type, and speed (the rest of the parameters are set
     * by default).
     * 
     * @param name Processor's name.
     * @param cu Computing units.
     * @param type Processor type CPU|GPU|FPGA.
     * @param speed Processor speed.
     */
    public Processor(String name, int cu, String type, float speed) {
        this.setName(name);
        this.setComputingUnits(cu);
        this.setType(type);
        this.setSpeed(speed);
    }

    /**
     * Instantiates a processor with a given name, computing units, speed and architecture (the rest of the parameters
     * are set by default).
     * 
     * @param name Processor's name.
     * @param cu Processor's number of computing units.
     * @param type Processor type CPU|GPU|FPGA.
     * @param internalMemory Processor internal memory.
     * @param speed Processor's speed.
     * @param arch Processor's architecture.
     */
    public Processor(String name, int cu, String type, float internalMemory, float speed, String arch) {
        this.setName(name);
        this.setComputingUnits(cu);
        this.setType(type);
        this.setInternalMemory(internalMemory);
        this.setSpeed(speed);
        this.setArchitecture(arch);
    }

    /**
     * Instantiates a processor with a given name, computing units, speed, architecture and processor property key-value
     * (the rest of the parameters are set by default).
     * 
     * @param name Processor's name.
     * @param cu Processor's number of computing units.
     * @param type Processor type CPU|GPU|FPGA
     * @param internalMemory Processor internal memory
     * @param speed Processor's speed.
     * @param arch Processor's architecture.
     * @param propName Internal processor's property name.
     * @param propValue Internal processor's property value.
     */
    public Processor(String name, int cu, String type, float internalMemory, float speed, String arch, String propName,
        String propValue) {
        this.setName(name);
        this.setComputingUnits(cu);
        this.setType(type);
        this.setInternalMemory(internalMemory);
        this.setSpeed(speed);
        this.setArchitecture(arch);
        this.setPropName(propName);
        this.setPropValue(propValue);
    }

    /**
     * Instantiates a processor with a given name, computing units and processor property key-value (the rest of the
     * parameters are set by default).
     * 
     * @param name Processor's name.
     * @param cu Processor's number of computing units.
     * @param propName Internal processor's property name.
     * @param propValue Internal processor's property value.
     */
    public Processor(String name, int cu, String propName, String propValue) {
        this.setName(name);
        this.setComputingUnits(cu);
        this.setPropName(propName);
        this.setPropValue(propValue);
    }

    /**
     * Instantiates a new processor copying the properties of the given processor.
     * 
     * @param p Processor to be cloned.
     */
    public Processor(Processor p) {
        this.setName(p.getName());
        this.setComputingUnits(p.getComputingUnits());
        this.setType(p.getType());
        this.setSpeed(p.getSpeed());
        this.setArchitecture(p.getArchitecture());
        this.setPropName(p.getPropName());
        this.setPropValue(p.getPropValue());
    }

    /**
     * Returns the name of the processor.
     * 
     * @return The processor's name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name of the processor.
     * 
     * @param name New processor's name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the number of computing units of the processor.
     * 
     * @return The number of computing units of the processor.
     */
    public int getComputingUnits() {
        return this.computingUnits;
    }

    /**
     * Sets the number of computing units of the processor.
     * 
     * @param computingUnits New number of computing units of the processor.
     */
    public void setComputingUnits(int computingUnits) {
        this.computingUnits = computingUnits;
    }

    /**
     * Adds a given amount {@code cu} of computing units to the current processor.
     * 
     * @param cu Computing Units to increase.
     */
    public void addComputingUnits(int cu) {
        this.computingUnits = this.computingUnits + cu;
    }

    /**
     * Removes a given amount {@code cu} of computing units to the current processor.
     * 
     * @param cu Computing Units to decrease.
     */
    public void removeComputingUnits(int cu) {
        this.computingUnits = this.computingUnits - cu;
    }

    /**
     * Scales the current computing units by a given factor.
     * 
     * @param amount Factor to multiply the current computing units.
     */
    public void multiply(int amount) {
        this.computingUnits = this.computingUnits * amount;
    }

    /**
     * Returns the processor's type.
     * 
     * @return The processor's type.
     */
    public String getType() {
        return this.type;
    }

    /**
     * Sets a new type to the processor.
     * 
     * @param type New processor's type.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Returns the internal memory size.
     * 
     * @return The internal memory size.
     */
    public float getInternalMemory() {
        return this.internalMemory;
    }

    /**
     * Sets a new internal memory size.
     * 
     * @param internalMemory New internal memory size.
     */
    public void setInternalMemory(float internalMemory) {
        this.internalMemory = internalMemory;
    }

    /**
     * Returns the processor's speed.
     * 
     * @return The processor's speed.
     */
    public float getSpeed() {
        return this.speed;
    }

    /**
     * Sets the processor's speed.
     * 
     * @param speed new processor's speed.
     */
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    /**
     * Returns the processor's architecture.
     * 
     * @return The processor's architecture.
     */
    public String getArchitecture() {
        return this.architecture;
    }

    /**
     * Sets the processor architecture.
     * 
     * @param architecture New processor's architecture.
     */
    public void setArchitecture(String architecture) {
        this.architecture = architecture;
    }

    /**
     * Returns the processor's property name.
     * 
     * @return The processor's property name.
     */
    public String getPropName() {
        return this.propName;
    }

    /**
     * Sets the processor property name.
     * 
     * @param propName New processor's property name.
     */
    public void setPropName(String propName) {
        this.propName = propName;
    }

    /**
     * Returns the processor's property value.
     * 
     * @return The processor's property value.
     */
    public String getPropValue() {
        return this.propValue;
    }

    /**
     * Sets the processor property value.
     * 
     * @param propValue New processor's property value.
     */
    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

}
