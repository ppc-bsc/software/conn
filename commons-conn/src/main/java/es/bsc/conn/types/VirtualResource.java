package es.bsc.conn.types;

import java.util.Map;


/**
 * Implementation of a Virtual Resource description.
 */
public class VirtualResource {

    private static final String DEFAULT_ID = "-1";
    private static final String INVALID_IP = "-1.-1.-1.-1";

    private Object id;
    private String ip;

    private HardwareDescription hd;
    private SoftwareDescription sd;

    private Map<String, String> properties;


    /**
     * Creates a new virtual resource with the default values.
     */
    public VirtualResource() {
        this.id = DEFAULT_ID;
        this.ip = INVALID_IP;

        this.hd = null;
        this.sd = null;

        this.properties = null;
    }

    /**
     * Creates a new virtual resource from the given values.
     * 
     * @param id Virtual resource associated Id.
     * @param hd Associated Hardware Description.
     * @param sd Associated Software Description.
     * @param prop Extra properties.
     */
    public VirtualResource(Object id, HardwareDescription hd, SoftwareDescription sd, Map<String, String> prop) {
        this.id = id;
        this.ip = INVALID_IP;

        this.hd = hd;
        this.sd = sd;

        this.properties = prop;
    }

    /**
     * Returns the Id.
     * 
     * @return VM Id.
     */
    public Object getId() {
        return this.id;
    }

    /**
     * Sets a new Id.
     * 
     * @param id New VM Id.
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     * Returns the associated IP address.
     * 
     * @return The associated IP address.
     */
    public String getIp() {
        return this.ip;
    }

    /**
     * Sets a new IP address.
     * 
     * @param ip New IP address.
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Returns the hardware description.
     * 
     * @return The hardware description.
     */
    public HardwareDescription getHd() {
        return this.hd;
    }

    /**
     * Sets the hardware description.
     * 
     * @param hd New hardware description.
     */
    public void setHd(HardwareDescription hd) {
        this.hd = hd;
    }

    /**
     * Returns the software description.
     * 
     * @return The software description.
     */
    public SoftwareDescription getSd() {
        return this.sd;
    }

    /**
     * Sets the software description.
     * 
     * @param sd New software description.
     */
    public void setSd(SoftwareDescription sd) {
        this.sd = sd;
    }

    /**
     * Returns the virtual resource properties.
     * 
     * @return The virtual resource properties.
     */
    public Map<String, String> getProperties() {
        return this.properties;
    }

    /**
     * Sets the virtual resource properties.
     * 
     * @param properties New Virtual Resource properties.
     */
    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

}
