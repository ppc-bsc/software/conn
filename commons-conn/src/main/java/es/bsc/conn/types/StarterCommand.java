package es.bsc.conn.types;

/**
 * Interface to describe the worker start command executed by the connector.
 */
public interface StarterCommand {

    /**
     * Returns the base working directory.
     * 
     * @return The base working directory.
     */
    public String getBaseWorkingDir();

    /**
     * Returns the complete worker start command to be executed in the worker machine.
     * 
     * @return The complete worker start command to be executed in the worker machine.
     * @throws Exception When an internal error occurs when building the worker command.
     */
    public String[] getStartCommand() throws Exception;

    /**
     * Sets a new worker name.
     * 
     * @param workerName New worker name.
     */
    public void setWorkerName(String workerName);

    /**
     * Sets a new node id (for tracing).
     * 
     * @param nodeId New node id.
     */
    public void setNodeId(String nodeId);

    /**
     * Sets a new sandboxed working directory.
     * 
     * @param sandboxedWorkingDir New sandboxed working directory.
     */
    public void setSandboxedWorkingDir(String sandboxedWorkingDir);

    /**
     * Sets a new script name.
     * 
     * @param scriptName New script name.
     */
    public void setScriptName(String scriptName);

}
