package es.bsc.conn.types;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * Implementation of a Virtual Resource Hardware Description.
 */
public class HardwareDescription {

    // Processor
    protected List<Processor> processors;
    // Processor CPU computing units
    protected int totalCPUComputingUnits = BasicTypes.ZERO_INT;
    protected int totalGPUComputingUnits = BasicTypes.ZERO_INT;
    protected int totalFPGAComputingUnits = BasicTypes.ZERO_INT;

    // Memory
    protected float memorySize = BasicTypes.UNASSIGNED_FLOAT;
    protected String memoryType = BasicTypes.UNASSIGNED_STR;

    // Storage
    protected float storageSize = BasicTypes.UNASSIGNED_FLOAT;
    protected String storageType = BasicTypes.UNASSIGNED_STR;

    // Price
    protected int priceTimeUnit = BasicTypes.UNASSIGNED_INT;
    protected float pricePerUnit = BasicTypes.UNASSIGNED_FLOAT;

    // Image
    protected String imageName;
    protected String type;
    protected Map<String, String> imageProperties;


    /**
     * Instantiates a default hardware description.
     */
    public HardwareDescription() {
        this.processors = new LinkedList<>();
        this.imageName = null;
        this.type = null;
        this.imageProperties = new HashMap<>();
    }

    /**
     * Instantiates a new hardware description with the given parameters.
     * 
     * @param processors List of processors.
     * @param totalCPUComputingUnits Total number of CPU computing units.
     * @param totalGPUComputingUnits Total number of GPU computing units.
     * @param totalFPGAComputingUnits Total number of FPGA computing units.
     * @param memorySize Total memory size.
     * @param memoryType Memory type.
     * @param storageSize Total storage size.
     * @param storageType Storage type.
     * @param priceTimeUnit Time unit for price computation.
     * @param pricePerUnit Price per time unit.
     * @param imageName Image name.
     * @param imageType Image type.
     * @param imageProperties Specific image properties.
     */
    public HardwareDescription(List<Processor> processors, int totalCPUComputingUnits, int totalGPUComputingUnits,
        int totalFPGAComputingUnits, float memorySize, String memoryType, float storageSize, String storageType,
        int priceTimeUnit, float pricePerUnit, String imageName, String imageType,
        Map<String, String> imageProperties) {

        this.processors = processors;
        this.totalCPUComputingUnits = totalCPUComputingUnits;
        this.totalGPUComputingUnits = totalGPUComputingUnits;
        this.memorySize = memorySize;
        this.memoryType = memoryType;
        this.storageSize = storageSize;
        this.storageType = storageType;
        this.priceTimeUnit = priceTimeUnit;
        this.pricePerUnit = pricePerUnit;
        this.imageName = imageName;
        this.type = imageType;
        this.imageProperties = imageProperties;
    }

    /**
     * Returns the processors' description.
     * 
     * @return The processors' description.
     */
    public List<Processor> getProcessors() {
        return this.processors;
    }

    /**
     * Sets the processors' description.
     * 
     * @param proc New processors' description.
     */
    public void setProcessors(List<Processor> proc) {
        this.processors = proc;
    }

    /**
     * Returns the total number of CPU computing units.
     * 
     * @return The total number of CPU computing units.
     */
    public int getTotalCPUComputingUnits() {
        return this.totalCPUComputingUnits;
    }

    /**
     * Sets the total number of CPU computing units.
     * 
     * @param tCU Total number of CPU computing units.
     */
    public void setTotalComputingUnits(int tCU) {
        this.totalCPUComputingUnits = tCU;
    }

    /**
     * Returns the total number of GPU computing units.
     * 
     * @return The total number of GPU computing units.
     */
    public int getTotalGPUComputingUnits() {
        return this.totalGPUComputingUnits;
    }

    /**
     * Sets the total number of GPU computing units.
     * 
     * @param tCU Total number of GPU computing units.
     */
    public void setTotalGPUComputingUnits(int tCU) {
        this.totalGPUComputingUnits = tCU;
    }

    /**
     * Returns the total number of FPGA computing units.
     * 
     * @return The total number of FPGA computing units.
     */
    public int getTotalFPGAComputingUnits() {
        return this.totalFPGAComputingUnits;
    }

    /**
     * Sets the total number of FPGA computing units.
     * 
     * @param tCU Total number of FPGA computing units.
     */
    public void setTotalFPGAComputingUnits(int tCU) {
        this.totalFPGAComputingUnits = tCU;
    }

    /**
     * Returns the memory size.
     * 
     * @return The memory size.
     */
    public float getMemorySize() {
        return this.memorySize;
    }

    /**
     * Sets the memory size.
     * 
     * @param memS New memory size.
     */
    public void setMemorySize(float memS) {
        this.memorySize = memS;
    }

    /**
     * Returns the memory type.
     * 
     * @return The memory type.
     */
    public String getMemoryType() {
        return this.memoryType;
    }

    /**
     * Sets the memory type.
     * 
     * @param memT New memory type.
     */
    public void setMemoryType(String memT) {
        this.memoryType = memT;
    }

    /**
     * Returns the storage size.
     * 
     * @return The storage size.
     */
    public float getStorageSize() {
        return this.storageSize;
    }

    /**
     * Sets the storage size.
     * 
     * @param strS New storage size.
     */
    public void setStorageSize(float strS) {
        this.storageSize = strS;
    }

    /**
     * Returns the storage type.
     * 
     * @return The storage type.
     */
    public String getStorageType() {
        return this.storageType;
    }

    /**
     * Sets the storage type.
     * 
     * @param strT New storage type.
     */
    public void setStorageType(String strT) {
        this.storageType = strT;
    }

    /**
     * Returns the price time unit.
     * 
     * @return The price time unit.
     */
    public int getPriceTimeUnit() {
        return this.priceTimeUnit;
    }

    /**
     * Sets the price time unit.
     * 
     * @param pTU New price time unit.
     */
    public void setPriceTimeUnit(int pTU) {
        this.priceTimeUnit = pTU;
    }

    /**
     * Returns the price per time unit.
     * 
     * @return The price per time unit.
     */
    public float getPricePerUnit() {
        return this.pricePerUnit;
    }

    /**
     * Sets the price per time unit.
     * 
     * @param pPU New price per time unit.
     */
    public void setPricePerUnit(float pPU) {
        this.pricePerUnit = pPU;
    }

    /**
     * Returns the image name.
     * 
     * @return The image name.
     */
    public String getImageName() {
        return this.imageName;
    }

    /**
     * Sets the image name.
     * 
     * @param imageName New image name.
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**
     * Returns the image type.
     * 
     * @return The image type.
     */
    public String getType() {
        return this.type;
    }

    /**
     * Sets the image type.
     * 
     * @param type New image type.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Returns the image specific properties.
     * 
     * @return The image specific properties.
     */
    public Map<String, String> getImageProperties() {
        return this.imageProperties;
    }

    /**
     * Sets the image specific properties.
     * 
     * @param imageProperties New image specific properties.
     */
    public void setImageProperties(Map<String, String> imageProperties) {
        this.imageProperties = imageProperties;
    }

    @Override
    public String toString() {
        // WARN: Does not display all the task fields
        StringBuilder sb = new StringBuilder();
        sb.append("HD-").append(hashCode());
        sb.append("[");
        sb.append("totalCPUs = ").append(this.totalCPUComputingUnits).append(",");
        sb.append("totalGPUs = ").append(this.totalGPUComputingUnits).append(",");
        sb.append("totalFPGAs = ").append(this.totalFPGAComputingUnits).append(",");
        sb.append("memorySize = ").append(this.memorySize).append(",");
        sb.append("memoryType = ").append(this.memoryType).append(",");
        sb.append("storageSize = ").append(this.storageSize).append(",");
        sb.append("storageType = ").append(this.storageType).append(",");
        sb.append("priceTimeUnit = ").append(this.priceTimeUnit).append(",");
        sb.append("pricePerUnit = ").append(this.pricePerUnit).append(",");
        sb.append("imageName = ").append(this.imageName).append(",");
        sb.append("type = ").append(this.type);
        sb.append("]");

        return sb.toString();
    }

}
